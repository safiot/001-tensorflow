### Formatoins

1.  https://tensorflow.devpost.com/
2.  https://www.fast.ai/
3.  https://www.deeplearning.ai/



### Youtube playlist
https://goo.gl/KewA03
#### Labs 
TensorFlow for Poets Codelab: https://goo.gl/QTwZ3v
#### Deep Learning
Google’s Udacity class on Deep Learning: https://goo.gl/iRqXsy
#### TensorFlow tutorial
https://goo.gl/0Oz7B5

#### Google Research blog on Inception
https://goo.gl/CSrfJ1

You can follow me on Twitter at https://twitter.com/random_forests for updates on episodes, and of course - Google Developers.

Subscribe to Google Developers: http://goo.gl/mQyv5L - 

Subscribe to the brand new Firebase Channel: https://goo.gl/9giPHG

#### IOS 
https://codelabs.developers.google.com/codelabs/tensorflow-for-poets-2-ios/#0


### Handwritten classifier 
https://www.youtube.com/watch?v=Gj0iyo265bc&index=7&list=PLOU2XLYxmsIIuiBfYad6rFYQU_jL2ryal

Last time we wrote an image classifier using TensorFlow for Poets. This time, we’ll write a basic one using TF.Learn. To make it easier for you to try this out, I wrote a Jupyter Notebook for this episode -- https://goo.gl/NNlMNu -- and I’ll start with a quick screencast of installing TensorFlow using Docker, and serving the notebook. This is a great way to get all the dependencies installed and properly configured. I've linked some additional notebooks below you can try out, too. Next, I’ll start introducing a linear classifier. My goal here is just to get us started. I’d like to spend a lot more time on this next episode, if there’s interest? I have a couple alternate ways of introducing them that I think would be helpful (and I put some exceptional links below for you to check out to learn more, esp. Colah's blog and CS231n - wow!). Finally, I’ll show you how to reproduce those nifty images of weights from TensorFlow.org's Basic MNIST’s tutorial.

Jupyter Notebook: https://goo.gl/NNlMNu

Docker images: https://goo.gl/8fmqVW

MNIST tutorial: https://goo.gl/GQ3t7n

Visualizing MNIST: http://goo.gl/ROcwpR (this blog is outstanding)

More notebooks: https://goo.gl/GgLIh7

More about linear classifiers: https://goo.gl/u2f2NE

Much more about linear classifiers: http://goo.gl/au1PdG (this course is outstanding, highly recommended)

More TF.Learn examples: https://goo.gl/szki63

Thanks for watching, and have fun! For updates on new episodes, you can find me on Twitter at www.twitter.com/random_forests